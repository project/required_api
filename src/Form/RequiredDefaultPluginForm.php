<?php

namespace Drupal\required_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure default required strategy for this site.
 */
class RequiredDefaultPluginForm extends ConfigFormBase {

  /**
   * The required plugin manager.
   *
   * @var \Drupal\required_api\RequiredManager
   */
  protected $requiredManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->requiredManager = $container->get('plugin.manager.required_api.required');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'required_default_plugin';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $plugins = $this->requiredManager->getDefinitionsAsOptions();
    $config = $this->configFactory->get('required_api.plugins');
    $plugin = $config->get('default_plugin');

    $form['default_plugin'] = [
      '#title' => $this->t('Default required strategy'),
      '#type' => 'radios',
      '#options' => $plugins,
      '#default_value' => $plugin,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('required_api.plugins');
    $config->set('default_plugin', $form_state->getValue('default_plugin'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'required_api.plugins',
    ];
  }

}
