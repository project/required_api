<?php

namespace Drupal\required_api;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages required by role plugins.
 */
class RequiredManager extends DefaultPluginManager {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new RequiredManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    ConfigFactoryInterface $configFactory,
  ) {
    parent::__construct(
      'Plugin/Required',
      $namespaces,
      $moduleHandler,
      'Drupal\required_api\Plugin\RequiredPluginInterface',
      'Drupal\required_api\Annotation\Required'
    );
    $this->setCacheBackend($cacheBackend, 'required_api_required_plugins');
    $this->configFactory = $configFactory;
  }

  /**
   * Overrides PluginManagerBase::getInstance().
   */
  public function getInstance(array $options) {
    $plugin_id = $options['plugin_id'] ?? $this->getPluginId($options['field_definition']);
    $options['plugin_id'] = $plugin_id;

    return $this->createInstance($plugin_id, $options);
  }

  /**
   * Gets the plugin_id for this field definition, fallback to system default.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   *   A field instance.
   *
   * @return string
   *   The plugin id.
   */
  public function getPluginId(FieldDefinitionInterface $field) {
    return $field->getThirdPartySetting('required_api', 'required_plugin', $this->getDefaultPluginId());
  }

  /**
   * Gets the default plugin_id for the system.
   *
   * @return string
   *   The plugin id.
   */
  public function getDefaultPluginId() {
    return $this->configFactory
      ->get('required_api.plugins')
      ->get('default_plugin');
  }

  /**
   * Provides the definition ids.
   */
  public function getDefinitionsIds() {
    return array_keys($this->getDefinitions());
  }

  /**
   * Provides the definitions as options just to inject to a select element.
   */
  public function getDefinitionsAsOptions() {
    $definitions = $this->getDefinitions();

    return array_map(function ($definition) {
      return $definition['label'];
    }, $definitions);
  }

}
