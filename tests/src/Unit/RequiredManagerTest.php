<?php

namespace Drupal\Tests\required_api\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\required_api\RequiredManager;

/**
 * Tests the breadcrumb manager.
 *
 * @group Drupal
 * @group Required API
 */
class RequiredManagerTest extends UnitTestCase {

  /**
   * The tested required manager.
   *
   * @var \Drupal\required_api\RequiredManager
   */
  protected $requiredManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $namespaces = new \ArrayObject([]);
    $cache_backend = $this->createMock('Drupal\Core\Cache\MemoryBackend');
    $module_handler = $this->createMock('Drupal\Core\Extension\ModuleHandlerInterface');
    $config_factory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');

    $this->requiredManager = new RequiredManager($namespaces, $cache_backend, $module_handler, $config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => 'Required manager',
      'description' => 'Tests the required manager.',
      'group' => 'Required API',
    ];
  }

  /**
   * Tests creating a Required Manager instance.
   */
  public function testCreateManagerInstance() {

    $is_object = is_object($this->requiredManager);
    $is_instance_of_required_manager = $this->requiredManager instanceof RequiredManager;

    $this->assertTrue($is_object, 'The requiredManager property is an object');
    $this->assertTrue($is_instance_of_required_manager, 'The requiredManager is instance of RequiredManager');

  }

}
