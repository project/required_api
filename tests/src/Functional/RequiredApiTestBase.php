<?php

namespace Drupal\Tests\required_api\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides common functionality for the Field UI test classes.
 */
abstract class RequiredApiTestBase extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'field_ui',
    'field_test',
    'required_api',
    'required_api_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create test user.
    $admin_user = $this->drupalCreateUser([
      'access content',
      'administer content types',
      'administer node fields',
      'administer node form display',
      'administer node display',
      'administer users',
      'administer account settings',
      'administer user display',
      'bypass node access',
      'administer required settings',
    ]);
    $this->drupalLogin($admin_user);

    // Create Article node type.
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);
  }

}
